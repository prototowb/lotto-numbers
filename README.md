# Lotto Numbers (lottopia)

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
pnpm install
```

### Run Proxy

```sh
pnpm run proxy
```

### Compile and Hot-Reload for Development

```sh
pnpm dev
```

### Run Tests

```sh
pnpm vitest
```
