import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import EuroJackpotCard from '@/components/EuroJackpot/EuroJackpotCard.vue';

describe('EuroJackpotCard Component', () => {
  const drawData = {
    date: '2024-05-06',
    jackpot: 50000000,
    numbers: [1, 2, 3, 4, 5],
    additionalNumbers: [6, 7]
  };

  it('disables the prev button if isFirstSlide is true', () => {
    const wrapper = mount(EuroJackpotCard, {
      props: {
        draw: drawData,
        isFirstSlide: true,
        isLastSlide: false
      }
    });

    expect(wrapper.find('button[aria-label="prev-slide"]').exists()).toBe(false);
  });

  it('disables the next button if isLastSlide is true', () => {
    const wrapper = mount(EuroJackpotCard, {
      props: {
        draw: drawData,
        isFirstSlide: false,
        isLastSlide: true
      }
    });

    expect(wrapper.find('button[aria-label="next-slide"]').exists()).toBe(false);
  });
});
