import { describe, it, expect, beforeEach, vi } from 'vitest';
import { ref } from 'vue';
import { flushPromises, mount } from '@vue/test-utils';
import EuroJackpotCarousel from '../EuroJackpotCarousel.vue';
import { useQuery } from '@vue/apollo-composable';

vi.mock('@vue/apollo-composable', () => ({
  useQuery: vi.fn()
}));

const mockResults = {
  draw: {
    draws: [
      {
        date: '2024-05-07',
        jackpot: 10000000,
        numbers: [1, 2, 3, 4, 5],
        additionalNumbers: [6, 7]
      }
    ]
  }
};

let mockUseQuery: ReturnType<typeof useQuery>;

beforeEach(() => {
  mockUseQuery = vi.mocked(useQuery);
  mockUseQuery.mockReturnValue({
    result: ref(mockResults),
    loading: ref(false),
    error: ref(null)
  });
});

describe('EuroJackpotCarousel Component', () => {
  it('displays EuroJackpotCard components when results exist', async () => {
    const wrapper = mount(EuroJackpotCarousel);
    await wrapper.vm.$nextTick();
    await flushPromises();

    const cards = wrapper.findAllComponents({ name: 'EuroJackpotCard' });
    expect(cards.length).toBe(1);
  });

  it('displays a loading message correctly', async () => {
    mockUseQuery.mockReturnValue({
      result: ref(null),
      loading: ref(true),
      error: ref(null)
    });

    const wrapper = mount(EuroJackpotCarousel);
    await flushPromises();

    expect(wrapper.html()).toContain('Loading... Please wait.');
  });
});
