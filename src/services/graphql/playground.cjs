const express = require('express');
const playgroundMiddleware = require('graphql-playground-middleware-express').default;

const app = express();
app.use('/playground', playgroundMiddleware({ endpoint: '/graphql' }));

app.listen(4000, () => {
  console.log('Server running on http://localhost:4000/playground');
});
