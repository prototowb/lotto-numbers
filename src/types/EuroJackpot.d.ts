export interface Draw {
  date: String;
  jackpot: number;
  numbers: Array<number>;
  additionalNumbers: Array<number>;
}

export interface DrawData {
  draws: Draw[];
}

export interface EuroJackpotResults {
  draw: DrawData | null;
}
