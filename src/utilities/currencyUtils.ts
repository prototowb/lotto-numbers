/**
 * Converts cents to a formatted string in dollars.
 * @param {number} cents - The amount in cents.
 * @param {number} decimalPlaces - Number of decimal places for formatting (default is 0).
 * @returns {string} The amount formatted as a dollar string.
 */

export const formatCentsToDollars = (cents: number, decimalPlaces = 0): string => {
  const dollars = cents / 100;

  // Format the number with comma separators and desired decimal places
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: decimalPlaces,
    maximumFractionDigits: decimalPlaces
  }).format(dollars);
};
